import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const IngredientList = (props) => {
    return (
        <Autocomplete
            id="combo-box"
            options={props.ingredients}
            onChange={(event, newValue) => {
                props.onChange(newValue);
            }}
            getOptionLabel={(option) => option}
            style={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Click to choose" variant="outlined" />}
        />
    );
}

export default IngredientList
