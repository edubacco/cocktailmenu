import React, {Component} from 'react';
import ReactDOM from "react-dom";
import IngredientList from './IngredientList';
import CocktailList from "./CocktailList";
import OrdersTable from "./OrdersTable";

class App extends Component {

    state = {
        ingredients: [],
        cocktails: [],
        choosedIngredient: '',
        order: {
            id: 123,
            status: "draft",
            cocktails: []
        }
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let uri = 'http://localhost:809/api/v1/ingredients';
        axios.get(uri).then((response) => {
            let state = this.state;
            state.ingredients = response.data;
            this.setState(state)
        });
    }

    setChoosedIngredient = (choosed) => {
        let state = this.state
        state.choosedIngredient = choosed
        this.setState(state);
        this.setCocktailsList()
    }

    setCocktailsList = () => {
        let uri = 'http://localhost:809/api/v1/drinks?ingredientName=' + this.state.choosedIngredient;
        axios.get(uri).then((response) => {
            let state = this.state;
            state.cocktails = response.data;
            this.setState(state)
        });
    }

    setOrder = (neworder) => {
        let state = this.state;
        state.order = neworder;
        this.setState(state)
    }

    addCocktailToOrder = (name) => {
        let state = this.state;
        state.order.cocktails.push({name: name})
        this.setState(state)
    }

    deleteCocktailFromOrder = (name) => {
        console.log("deleteCocktailFromOrder")
        let state = this.state;
        let newRows = state.order.cocktails.filter(function (value, index, arr) {
            //todo: this approach fails on handling duplicate elements. Change it!
            return value.name !== name;
        });
        state.order.cocktails = newRows
        this.setState(state)

    }

    confirmOrder = () => {
        let uri = 'http://localhost:809/api/v1/order';
        let payload = {drinks: JSON.stringify(this.state.cocktails)};
        axios.post(uri, payload).then((response) => {
        });
    }

    render() {
        return (
            <div className="container">
                <div className="card">
                    <div className="card-header">
                        Choose your favourite ingredient! (They are {this.state.ingredients.length}). {this.state.choosedIngredient}
                    </div>
                    <div className="card-body">
                        <IngredientList ingredients={this.state.ingredients} onChange={this.setChoosedIngredient}/>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header">
                        Then click on cocktails to add them to your order
                    </div>
                    <div className="card-body">
                        <CocktailList cocktailList={this.state.cocktails} order={this.state.order}
                                      onAddCocktail={this.addCocktailToOrder}/>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header">
                        Finally review and confirm your order!
                    </div>
                    <div className="card-body">
                        <OrdersTable order={this.state.order} onDeleteCocktail={this.deleteCocktailFromOrder}
                                     onConfirmOrder={this.confirmOrder}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App/>, document.getElementById('app'));
}
