import React from 'react';
import {lighten, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';


const headCells = [
    {id: 'Cocktail', numeric: false, disablePadding: true, label: 'Cocktail'},
    {id: 'delete', numeric: false, disablePadding: false, label: ''},
];

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));


export default function OrdersTable(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [buttonVisible, setButtonVisible] = React.useState(false);

    const handleConfirmOrder = () => {
        props.onConfirmOrder();
        setOpen(true);
    };

    const handleModalClose = () => {
        //todo: empty the order
        setOpen(false);
    };

    if (props.order.cocktails.length > 0 && !buttonVisible) {
        setButtonVisible(true);
    } else if (props.order.cocktails.length == 0 && buttonVisible) {
        setButtonVisible(false);
    }

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={'medium'}
                        aria-label="enhanced table"
                    >
                        <TableBody>
                            {props.order.cocktails.map((row, index) => {
                                return (
                                    <TableRow
                                        hover
                                        onClick={(event) => props.onDeleteCocktail(row.name)}
                                        // tabIndex={-1}
                                        key={index}
                                    >
                                        <TableCell align="left">{row.name}</TableCell>
                                        <TableCell padding="checkbox">
                                            <Tooltip title="Remove from order">
                                                <IconButton aria-label="delete">
                                                    <DeleteIcon/>
                                                </IconButton>
                                            </Tooltip>

                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            {buttonVisible ? (
                <button type="button" onClick={handleConfirmOrder}>
                    Confirm Order
                </button>            ) : null}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleModalClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2 id="transition-modal-title">Order confirmed!</h2>
                        <p id="transition-modal-description">Your cocktails will be there in some minute</p>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
