import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/AddCircle'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 650,
        height: 500,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

export default function CocktailList(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <GridList cellHeight={200} className={classes.gridList} cols={3}>
                {props.cocktailList.map((drink) => (
                    <GridListTile key={drink.idDrink} onClick={(event) => props.onAddCocktail(drink.strDrink)}>
                        <img src={drink.strDrinkThumb} alt={drink.strDrink} />
                        <GridListTileBar
                            title={drink.strDrink}
                            subtitle={<span>id: {drink.idDrink}</span>}
                            actionIcon={
                                <IconButton aria-label={`info about ${drink.title}`} className={classes.icon}>
                                    <AddIcon />
                                </IconButton>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}
