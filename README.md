# TabletMenu

# Dev env
Included here there is a development environment based on [laradock.io](https://laradock.io/)

## Prerequisites
to run the development environment you must have installed:
- docker
- docker-compose

## Usage:
run

`./laradock.sh up`

vist

`http://localhost:809` for the app

`http://localhost:8089` for phpmyadmin (host: mysql, user: root, psw: root)

## commands:
`./laradock.sh install` Install the dev env

`./laradock.sh up` Start the dev env

`./laradock.sh stop` Stop the dev env

`./laradock.sh down` Delete containers of the dev env

`./laradock.sh status` Show containers status

`./laradock.sh shell` Create a shell for the workspace

`./laradock.sh logs` Tail logs

