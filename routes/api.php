<?php

use App\Services\TheCocktailDb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//we will think later to authentication...
/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => 'api', 'namespace' => 'Api\v1', 'prefix' => 'v1'], function () {
    Route::get('/ingredients', 'TheCocktailDbController@indexIngredients');
    Route::get('/drinks', 'TheCocktailDbController@indexDrinks');

    Route::post('/order', 'OrderController@store');
});
