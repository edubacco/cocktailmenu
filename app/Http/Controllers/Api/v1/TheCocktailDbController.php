<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\TheCocktailDb;
use Illuminate\Http\Request;

class TheCocktailDbController extends Controller
{
    protected TheCocktailDb $theCocktailDbService;

    public function __construct(TheCocktailDb $theCocktailDbService)
    {
        $this->theCocktailDbService = $theCocktailDbService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexIngredients()
    {
        $ing = [];
        $ingredNames = $this->theCocktailDbService->ingredientsList();
        foreach ($ingredNames as $ingName) {
            //no need for more details..
//            $ing[] = $this->theCocktailDbService->ingredientByName($ingName->strIngredient1);
            $ing[] = $ingName->strIngredient1;
        }

        return response()->json($ing);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexDrinks(Request $request)
    {
        $ingredientName = $request->input('ingredientName');

        $drinks = $this->theCocktailDbService->cocktailByIngredientName($ingredientName);
        return response()->json($drinks);
    }
}
