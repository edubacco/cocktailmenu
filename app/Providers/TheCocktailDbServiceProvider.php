<?php

namespace App\Providers;

use App\Services\TheCocktailDb;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class TheCocktailDbServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TheCocktailDb::class, function () {
            $client = new Client(['base_uri' => TheCocktailDb::baseUri]);
            return new TheCocktailDb(
                $client,
                config('app.thecocktaildb.apikey')
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
