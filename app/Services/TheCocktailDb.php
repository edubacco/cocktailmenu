<?php


namespace App\Services;


use GuzzleHttp\Client;

/**
 * Class TheCocktailDb
 *
 * @package tabletmenu
 * @author eduardo3q@gmail.com
 */
class TheCocktailDb
{
    const baseUri = 'https://www.thecocktaildb.com';
    const prefix = 'api/json/v1';

    /**
     * @var string
     */
    private string $apiKey;

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * TheCocktailDb constructor.
     * @param  Client  $client
     * @param  string  $apiKey
     */
    public function __construct(Client $client, string $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    public function ingredientsList()
    {
        $endpoint = "list.php?i=list";
        $data = $this->getData($endpoint);
        return $data->drinks;
    }

    public function ingredientByName(string $name)
    {
        $endpoint = "search.php?i=$name";
        $data = $this->getData($endpoint);
        return $data->ingredients;
    }

    public function ingredientById(int $id)
    {
        $endpoint = "lookup.php?iid=$id";
        $data = $this->getData($endpoint);
        return $data->ingredients;
    }

    public function cocktailByName(string $name)
    {
        $endpoint = "search.php?s=$name";
        $data = $this->getData($endpoint);
        return $data->drinks;
    }

    public function cocktailByFirstLetter(string $letter)
    {
        $endpoint = "search.php?f=$letter";
        $data = $this->getData($endpoint);
        return $data->drinks;
    }

    public function cocktailByIngredientName(string $ingredientName)
    {
        $endpoint = "filter.php?i=$ingredientName";
        $data = $this->getData($endpoint);
        return $data->drinks ?? [];
    }

    public function cocktailDetailsById(int $id)
    {
        $endpoint = "lookup.php?i=$id";
        $data = $this->getData($endpoint);
        return $data->drinks;
    }

    protected function getData($endpoint)
    {
        $response = $this->client->get(self::prefix."/".$this->apiKey."/".$endpoint);
        return \GuzzleHttp\json_decode($response->getBody());
    }
}
