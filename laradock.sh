#!/bin/bash
set -e

abspath=$(pwd)
abspath_laradock=${abspath}/laradock
abspath_project=${abspath}

YELLOW="$(echo -e "\033[33m")" # Giallo Bold
BLUE="$(echo -e "\033[36m")"   # Azzurro Bold
GREEN="$(echo -e "\033[32m")"  # Verde Bold
RED="$(echo -e "\033[31m")"    # Rosso Bold
GRAY="$(echo -e "\033[37m")"   # Grey
RESET="$(echo -e "\033[0m")"   # Reset

function log_error() {
    msg=$1
    echo -e "${RED}${msg}${RESET}"
}

function log_warning() {
    msg=$1
    echo -e "${YELLOW}${msg}${RESET}"
}

function log_success() {
    msg=$1
    echo -e "${GREEN}${msg}${RESET}\n"
}

function log_info() {
    msg=$1
    echo -e "\n${BLUE}${msg}${RESET}"
}

function log() {
    msg=$1
    echo -e "${msg}${RESET}"
}

function replace_in_file() {
    if [[ "$CURRENT_OS" == 'Darwin' ]]; then
        # for MacOS
        sed -i '' -e "$1" "$2"
    else
        # for Linux
        sed -i "$1" "$2"
    fi
}

function checkDockerInstalled() {
    log_info "checking if docker exists..."
    if [[ ! -x "$(command -v docker)" ]]; then
        log_error "Docker must be installed!"
        exit 1
    else
        log_success "docker found"
    fi
    log_info "checking if docker-compose exists..."
    if [[ ! -x "$(command -v docker-compose)" ]]; then
        log_error "Docker compose must be installed!"
        exit 1
    else
        log_success "docker compose found"
    fi
}

function installLaradock() {
    #checkout laradock
    log_info "checking if laradock folder exists..."
    if [[ ! -d "${abspath}/laradock" ]]; then
        log "laradock not found, downloading"
        git clone https://github.com/Laradock/laradock.git
        log_success "laradock installed"
    else
        log_success "laradock already installed"
    fi
}

function setupLaradock() {
    log_info "# setting up laradock..."
    log "creating the .env file..."
    cp "${abspath_laradock}/env-example" "${abspath_laradock}/.env"

    log "change config in .env file..."
    replace_in_file 's/COMPOSE_PROJECT_NAME.*/COMPOSE_PROJECT_NAME=tabletmenu/' "${abspath_laradock}/.env"
    replace_in_file "s#DATA_PATH_HOST=.*#DATA_PATH_HOST=~/.tabletmenu/data#" "${abspath_laradock}/.env"
    replace_in_file "s#APACHE_DOCUMENT_ROOT.*#APACHE_DOCUMENT_ROOT=/var/www/public#" "${abspath_laradock}/.env"
    replace_in_file "s/APACHE_HOST_HTTP_PORT.*/APACHE_HOST_HTTP_PORT=809/" "${abspath_laradock}/.env"
    replace_in_file "s/APACHE_HOST_HTTPS_PORT.*/APACHE_HOST_HTTPS_PORT=4439/" "${abspath_laradock}/.env"
    replace_in_file "s/MYSQL_PORT.*/MYSQL_PORT=33069/" "${abspath_laradock}/.env"
    replace_in_file "s#WORKSPACE_INSTALL_MYSQL_CLIENT=.*#WORKSPACE_INSTALL_MYSQL_CLIENT=true#" "${abspath_laradock}/.env"
    replace_in_file "s/PMA_PORT.*/PMA_PORT=8089/" "${abspath_laradock}/.env"
    replace_in_file "s#PHP_FPM_INSTALL_XDEBUG=.*#PHP_FPM_INSTALL_XDEBUG=true#" "${abspath_laradock}/.env"
    replace_in_file "s#WORKSPACE_INSTALL_GIT_PROMPT=.*#WORKSPACE_INSTALL_GIT_PROMPT=true#" "${abspath_laradock}/.env"
    replace_in_file "s#SHELL_OH_MY_ZSH=.*#SHELL_OH_MY_ZSH=true#" "${abspath_laradock}/.env"

    replace_in_file "s#WORKSPACE_PUID=.*#WORKSPACE_PUID=$(id -u)#" "${abspath_laradock}/.env"
    replace_in_file "s#WORKSPACE_PGID=.*#WORKSPACE_PGID=$(id -g)#" "${abspath_laradock}/.env"
    replace_in_file "s#PHP_FPM_PUID=.*#PHP_FPM_PUID=$(id -u)#" "${abspath_laradock}/.env"
    replace_in_file "s#PHP_FPM_PGID=.*#PHP_FPM_PGID=$(id -g)#" "${abspath_laradock}/.env"
    replace_in_file "s#PHP_VERSION=.*#PHP_VERSION=7.4#" "${abspath_laradock}/.env"

    log_success "All done!"
}

function setupAppLaravel() {
    log_info "# running migrations..."
    cd "${abspath_laradock}"
    docker-compose run --rm php-fpm php artisan migrate
    cd -
    log_success "All done!"
}

case "$1" in
up)
    #maybe install
    if [ ! -f laradock/.env ]; then
        ./laradock.sh install
    fi
    cd laradock
    docker-compose up -d apache2 mysql phpmyadmin
    cd -
    ;;

stop)
    cd laradock
    docker-compose stop
    cd -
    ;;

down)
    cd laradock
    docker-compose down
    cd -
    ;;

status)
    cd laradock
    docker-compose status
    cd -
    ;;

shell)
    cd laradock
    docker-compose exec -u laradock workspace bash
    cd -
    ;;

logs)
    cd laradock
    docker-compose logs -f
    cd -
    ;;

install)
    checkDockerInstalled
    installLaradock
    setupLaradock
    setupAppLaravel
    cp .env.example .env
    ;;

*)
    echo $"Usage: $0 {up|stop|down|install}"
    exit 1
    ;;

esac
